from django.urls import path
from . import views

app_name = 'calculator'

urlpatterns = [
    path('addition/', views.Addition.as_view(), name='addition'),
]