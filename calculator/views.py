from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from django.contrib.auth.models import User
from rest_framework import status
# from braces.views import CsrfExemptMixin

# Create your views here.
class Addition(APIView):
    # authentication_classes = [authentication.TokenAuthentication]
    # permission_classes = [permissions.IsAdminUser]
    # authentication_classes = []

    def get(self, request, format=None):

        try:
            numbers = request.data
            num1 = int(numbers["num1"])
            num2 = int(numbers["num2"])

            #for testing, should return status 200 ok with 3
            # num1 = 1
            # num2 = 2

            answer = num1 + num2
            return Response(answer)
        
        except KeyError or ValueError:
            # return Response("error 1")
            return Response({}, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        except:
            # return Response("error 2")
            return Response({}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
